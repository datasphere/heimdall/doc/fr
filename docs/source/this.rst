.. index::
   single: introduction
.. |date| date::
.. _how_to_use_this:

============================
utiliser cette documentation
============================

L'objectif de cette documentation est de vous aider dans vos recherches.
Il s'adresse à toute personne intéressée par la gestion des données en général, et aux données de recherche en particulier.

Cette documentation est disponible sous plusieurs formes :
celle d'un :ref:`site web <website>`, et celle d'un :ref:`livre <book>`.
Quelle que soit sa forme, elle publiée sous les termes de la licence |copyright|.

.. index::
   single: site web
.. _website:

... via le site web
===================

Vous pouvez naviguer à travers ce site de différentes manières.
Choisissez celle(s) qui vous convien(nen)t le mieux !

#. Vous pouvez découvrir ce site comme on feuillette les pages d'un livre, en utilisant les boutons « Précédent » et « Suivant ».
   Ils se trouvent à priori en bas de chaque page.
#. Vous pouvez accéder directement à la section qui vous intéresse, grâce à la table des matières.
   Suivant votre appareil, elle se trouve à gauche de l'écran, ou dans la barre de menu en haut de l'écran.
   Vous pouvez aussi :ref:`la parcourir directement ici <toc>`.
#. Si vous avez besoin d'un index, :ref:`il se trouve par ici <genindex>`.
#. Vous pouvez aussi vous laisser guider par :ref:`cette liste des questions fréquentes <faq>`.
#. Vous pouvez rechercher directement une information grâce au champ de recherche.
   Il se trouve en haut à gauche de votre écran.
#. Si vous préférez consulter cette documentation dans une autre langue, utilisez le « `menu Read the Docs <https://docs.readthedocs.io/en/stable/flyout-menu.html>`_ ».
   Suivant votre appareil, celui-ci se trouve en bas à gauche de votre écran, ou en bas du menu qui s'ouvre lorsque vous cliquez en haut à gauche de votre écran.

Évidemment, ce site est accessible quel que soit votre appareil : PC, smartphone, tablette, ...

.. index::
   single: livre
   single: EPUB
   single: PDF
.. _book:

... sous forme de livre
=======================

Cette documentation est disponible aux formats **PDF**, **EPUB** et **HTML (zippé)**.
Vous pouvez téléchargez ces différentes versions via le « `menu Read the Docs <https://docs.readthedocs.io/en/stable/flyout-menu.html>`_ ».
Bonne lecture !

#. La table des matières commence :ref:`à cette page <toc>`.
#. L'index commence :ref:`à cette page <genindex>`.
#. La liste des questions les plus fréquentes commence :ref:`à cette page <faq>`.

Vous pouvez lire, citer, imprimer et redistribuer cette documentation en respectant les termes de la licence |copyright|.

.. note::
   Pour télécharger cette documentation au format livre depuis `le site web <https://datasphere.readthedocs.io/>`_, utilisez le « `menu Read the Docs <https://docs.readthedocs.io/en/stable/flyout-menu.html>`_ ».
   Suivant votre appareil, celui-ci se trouve en bas à gauche de votre écran, ou en bas du menu qui s'ouvre lorsque vous cliquez en haut à gauche de votre écran.

.. index::
   single: versions
   single: dépôt
   single: code
   single: source

dernière version
================

.. role:: raw-html(raw)
   :format: html

Vous lisez actuellement la version |version| de cette documentation, datée du |date|.
:raw-html:`<br/>`
Concernant la dernière version :

- Elle se trouve à ces adresses :
  :raw-html:`<br/>`
  |website|
  :raw-html:`<br/>`
  |mirror| (site miroir)
- Elle est téléchargeable en différents formats à cette adresse:
  :raw-html:`<br/>`
  |versions|
- Ses sources sont disponibles à cette adresse :
  :raw-html:`<br/>`
  |repo|

 ｡◕‿◕｡ ♪♫
