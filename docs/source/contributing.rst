.. index::
   single: contribuer
   single: aide
.. _contribuer:


Contribuer
==========

Tout d'abord, merci d'envisager de contribuer à |project|.
Veuillez prendre le temps de lire ce document afin de rendre vos contributions plus faciles pour toutes les personnes impliquées.

Suivre ces recommandations montrera votre respect des autres contributeurs.
En retour, ils vous rendront la pareille en essayant de résoudre votre problème, en évaluant vos changements ou en vous aidant dans la mesure du possible.

|project| est un logiciel libre et nous aimons recevoir des contributions de notre communauté —vous !
Il existe plusieurs manières de contribuer, depuis l'écriture de documentation, jusqu'au signalement d'anomalies, la demande de fonctionnalités ou l'amélioration du code de :ref:`pyHeimdall` ou `d'un de ses connecteurs <https://gitlab.huma-num.fr/datasphere/heimdall/connectors>`_, ou même de :ref:`xHeimdall`.
Aucun engagement sur la durée n'est requis ; même si vous ne faites que signaler une anomalie, donner votre avis ou corriger une coquille, vous êtes une contributrice ou un contributeur.

Les contributions sont régies par notre code de conduite : respect, collaboration, sobriété et, plus généralement, ne pas prendre les autres pour des cons.
Voici `des exemples plus spécifiques <https://policies.python.org/python.org/code-of-conduct/>`_, si vous êtes *ce genre* de personne.



Règles de base
--------------

Ce qui suit est une version traduite et générique du fichier ``CONTRIBUTING.rst`` de chaque projet.
N'hésitez pas à les consulter pour obtenir des informations plus précises :

- **pyHeimdall**: |repo_py|
- **xHeimdall**: |repo_xq|



Poser une question, signaler une anomalie
*****************************************

Merci de *ne pas* nous envoyer d'email pour poser des questions, signaler un bug ou toute autre activité sociale relative au projet.
Afin de rester `FAIR <https://www.go-fair.org/fair-principles/>`_, et pour ne pas augmenter la charge de travail des autres contributeurs, tous ces échanges devraient se dérouler `sous forme de tickets publics <https://gitlab.huma-num.fr/datasphere/heimdall/python/-/issues/>`_.

En `créant un nouveau ticket relatif à pyHeimdall <https://gitlab.huma-num.fr/datasphere/heimdall/python/-/issues/new>`_, assurez-vous de répondre aux cinq questions suivantes :

#. Quelle est votre version de Python (``python --version``)?
#. Utilisez-vous bien la dernière version d'|project| (``pip install --upgrade .``)?
#. Qu'avez-vous essayé de faire ?
#. À quoi vous attendiez-vous ?
#. Qu'avez-vous observé à la place ?

Bien sûr, en `créant un nouveau ticket relatif à xHeimdall <https://gitlab.huma-num.fr/datasphere/heimdall/xquery/-/issues/new>`_, indiquer la version de votre environnement XQuery/BaseX est plus pertinent.



Modifier le logiciel
********************

Toute proposition d'amélioration est la bienvenue sous forme de `merge request GitLab <https://docs.gitlab.com/ee/user/project/merge_requests/>`_.
Si vous découvrez ce projet et recherchez un moyen de vous impliquer, essayez de récupérer `un des tickets libellés « up-for-grabs » <https://gitlab.huma-num.fr/groups/datasphere/heimdall/-/issues/?sort=created_date&state=all&label_name%5B%5D=up-for-grabs>`_.
Des indications sur la tâche à realiser sont généralement données.

Un autre endroit pour trouver (ou confirmer) une envie est `la feuille de route de chaque projet <https://gitlab.huma-num.fr/groups/datasphere/heimdall/-/milestones>`_.

Pour toute vos merge requests, merci de respecter ce qui suit :

- Chaque merge request devrait contenir *une seule* fonctionnalité ou correction.
  Si vous désirez ajouter ou corriger plus d'une chose, soumettez autant de requêtes indépendantes.
- Gardez votre soumission simple et indépendante.
  N'ajoutez rien qui ne soit pas absolument nécessaire.
  Par exemple, privilégiez les fonctions simples aux grosses classes monolithiques.
  Modifiez uniquement les fichiers qui ont à voir avec le sujet de votre requête.
- N'ajoutez aucune nouvelle dépendance au projet.
- Assurez la compatibilité entre les autres versions du logiciel ou des plateformes sur lesquelles il s'exécute.
- Veillez à coder dans un style conforme à l'existant.
- Spécifiez de manière exhaustive le comportement de votre code à travers des tests automatiques, et ne « cassez » aucun test existant.
- N'oubliez pas d'accepter le `Certificat d'Origine du Développeur (DCO) <https://en.wikipedia.org/wiki/Developer_Certificate_of_Origin>`_ dans chacun de vos commits.
  Cela peut être fait facilement en utilisant l'option ``--signoff`` `dans vos commits Git <https://stackoverflow.com/questions/1962094/what-is-the-sign-off-feature-in-git-for>`_.

N'hésitez pas à demander de l'aide.
Tout le monde a été débutant un jour !



Pour aller plus loin
--------------------

- `Documentation de Heimdall <https://datasphere.readthedocs.io/projects/heimdall/>`_
- `Feuille de route de Heimdall <https://gitlab.huma-num.fr/groups/datasphere/heimdall/-/milestones>`_
- `Guidelines for non-code contributions to open source projects <https://opensource.com/life/16/1/8-ways-contribute-open-source-without-writing-code>`_
- `Working Open Guide: Mechanics of Contributing <https://mozillascience.github.io/leadership-training/03.1-mechanics.html>`_
- `First timers only <https://www.firsttimersonly.com/>`_

Les 2 dernières ressources de la liste parlent de GitHub.
Dans un contexte de logiciels de recherche, GitHub est juste une version de GitLab en tout point inférieure, coûteuse, et qui vous prive de vos libertés.
La majorité de ce que vous lirez dans ces pages sur GitHub s'applique aussi à GitLab.
