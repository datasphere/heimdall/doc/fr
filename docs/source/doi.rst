

.. |doi-pyheimdall| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.12795206.svg
   :target: https://doi.org/10.5281/zenodo.12795206
   :alt: Digital Object Identifier (DOI) de pyHeimdall

.. |doi-xheimdall| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.10638084.svg
   :target: https://doi.org/10.5281/zenodo.10638084
   :alt: Digital Object Identifier (DOI) de xHeimdall
