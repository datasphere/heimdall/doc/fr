.. _home:

#########
|project|
#########

|project| est un outil permettant de convertir facilement une ou plusieurs bases de données d'un format à un autre.

Il rend *techniquement* bien plus simple la migration et l'interopérabilité entre les bases de données SQL (`MariaDB <https://mariadb.org/>`_, `MySQL <https://www.mysql.com/fr/>`_), celles disposant d'une `API REST <https://fr.wikipedia.org/wiki/Representational_state_transfer>`_, ou les systèmes de gestion de bases de données relationnelles permettant l'import/export de fichiers au format `CSV <https://fr.wikipedia.org/wiki/Comma-separated_values>`_ (tels que les `tableurs <https://www.libreoffice.org/discover/calc/>`_ ou `Omeka <https://omeka.org/s/>`_) ou `XML <https://fr.wikipedia.org/wiki/Extensible_Markup_Language>`_.



Cas d'utilisation
=================

Si vous êtes, par exemple, dans une des situations suivantes …

* vous avez besoin d'accéder à des données, mais elle ne sont pas stockées dans un format compatible avec votre logiciel favori, …
* vous préféreriez unifier plusieurs bases de données hétérogènes en un seul corpus plus facile à analyser, …
* vous vous demandez quel format d'entrée privilégier pour votre logiciel de traitement ou votre base de données, …
* vous désirez rendre vos données plus accessibles et interopérables, …

… alors, |project| peut vous proposer un format d'échange, et abstraire les détails d'implémentation des données : les  vôtres, mais aussi celles des autres.
Grâce à |project|, vous pouvez aussi passer rapidement d'une technologie à l'autre au gré de vos besoins, sans jamais perdre, désorganiser ou corrompre vos données –voire, en les améliorant au fil du temps, en les documentant et en les adossant aux vocabulaires et ontologies de votre domaine.

En résumé, |project| (**H**\ *igh-*\ **E**\ *nd* **I**\ *nteroperability* **M**\ *odule when* **D**\ *ata is* **ALL** *over the place*) peut constituer un pont entre des îlots de données hétérogènes ou dispersées. ⚔️🌉🌈


En pratique
===========

.. index::
   single: versions
   single: implementations
.. _version:

|project| propose différentes implémentations de la :external+hera:doc:`modélisation HERA <design/concepts>`.
Chacune de ces implémentations, nommée *connecteurs*, permet l'import/export depuis/vers un format donné.
Tous ensemble, ces connecteurs rendent plus aisée (grâce à des fonctions utilitaires appuyées par un vocabulaire commun) et pérenne(grâce à une :external+datasphere:doc:`architecture modulaire <design/modularity>` et une :external+datasphere:doc:`gestion de projet résiliente <design/resilience>`) l’interopérabilité entre de multiples systèmes.

Il existe deux versions du logiciel |project| :

* |thumbnail-pyheimdall| :ref:`pyheimdall`, destinée à l'écosystème **Python**
* |thumbnail-xheimdall| :ref:`xheimdall`, destinée à l'écosystème **XQuery**

| Ces deux versions sont aussi compatibles que possible, mais leur périmètre fonctionnel n'est pas le même.
  En résumé, :ref:`xheimdall` est la version historique, confidentielle mais apparemment suffisante pour le peu de besoins exprimés relatifs à XQuery.
  De son coté, :ref:`pyheimdall` est plus moderne et offre un panel de fonctionnalités bien plus large, ainsi que davantage de connecteurs.
| Consultez la section dédiée à chaque version pour davantage de détails.


.. toctree::
   :hidden:

   /this
   /contributing
   /python/index
   /xquery/index
   /faq
   /genindex
   /search

.. include:: /doi.rst
.. include:: /thumbs.rst