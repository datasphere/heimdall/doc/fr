# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys
sys.path.insert(0, os.path.abspath('../../python/src'))

# -- Project information -----------------------------------------------------
language = 'fr'

project = 'HEIMDALL'
slug = 'heimdall'
group = 'datasphere'
gitlab = 'gitlab.huma-num.fr'
website = f'https://{group}.readthedocs.io/projects/{slug}'
mirror = f'https://{group}.gitpages.huma-num.fr/{slug}/doc/{language}/'
versions = f'https://readthedocs.org/projects/{slug}/downloads/'
doc = f'https://{gitlab}/{group}/{slug}/doc/{language}'
repo = f'https://{gitlab}/{group}/{slug}'
repo_py = f'{repo}/python/'
repo_xq = f'{repo}/xquery/'
issues = f'https://{gitlab}/groups/{group}/{slug}/-/issues/'
roadmap = f'https://{gitlab}/groups/{group}/{slug}/-/milestones/'

copyright = "CC BY-NC-SA 3.0"
author = 'Régis Witz'

release = '1.0'
version = '1.0.0'

rst_prolog = """
.. |project| replace:: {project}
.. |website| replace:: {website}
.. |mirror| replace:: {mirror}
.. |repo| replace:: {repo}
.. |version| replace:: {version}
.. |versions| replace:: {versions}
.. |repo_py| replace:: {repo_py}
.. |repo_xq| replace:: {repo_xq}
.. |issues| replace:: {issues}
.. |roadmap| replace:: {roadmap}
.. |copyright| replace:: {copyright}
""".format(
  project=project,
  website=website,
  mirror=mirror,
  repo=repo,
  repo_py=repo_py,
  repo_xq=repo_xq,
  version=version,
  versions=versions,
  issues=issues,
  roadmap=roadmap,
  copyright=copyright,
)

# -- General configuration ---------------------------------------------------

extensions = [
  'sphinx.ext.autodoc',       # HTML generation from docstrings
  'sphinx.ext.intersphinx',   # Link to other projects’ documentation
]

intersphinx_mapping = {
  'hera': ('https://datasphere.readthedocs.io/projects/hera/fr/latest', None),
  'hecate': ('https://datasphere.readthedocs.io/projects/hecate/fr/latest', None),
  'heimdall-en': ('https://datasphere.readthedocs.io/projects/heimdall/en/latest', None),
  'datasphere': ('https://datasphere.readthedocs.io/fr/latest', None),

  'python': ('https://docs.python.org/3/', None),
  'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output -------------------------------------------------

html_theme = 'sphinx_rtd_theme'

# see: https://sphinx-rtd-theme.readthedocs.io/en/stable/configuring.html
html_static_path = ['../resource']
html_css_files = ['css/candy.css']
html_logo = 'https://sharedocs.huma-num.fr/wl/?id=bTXaJHOJwSa2swWiqdHxHPbfqMouu38u&fmode=download'
html_theme_options = {
  'logo_only': True,
  'style_external_links': True,
}

# -- Options for EPUB output -------------------------------------------------
epub_show_urls = 'footnote'
