.. index::
   double: versions; pyheimdall
   single: pyheimdall
.. _pyheimdall:



pyHeimdall
==========

|doi-pyheimdall|

**pyHeimdall** est une implementation de :ref:`Heimdall <home>` en `langage Python <https://www.python.org/>`_.

.. seealso::

   Il existe un autre implémentation de Heimdall en language XQuery : :ref:`xheimdall`.
   Si vous avez besoin d'utiliser Heimdall depuis un environnement BaseX par exemple, :ref:`xheimdall` peut être plus appropriée.



.. _pyheimdall_install:

Installation
------------

.. image:: https://img.shields.io/pypi/v/pyheimdall
   :target: https://pypi.org/project/pyheimdall/
   :alt: PyPI - Python Version

| Ce logiciel est disponible en tant que `paquet PyPI <https://pypi.org/project/pyheimdall/>`_.
| Il peut donc être installé via le gestionnaire de paquets `pip <https://pip.pypa.io/en/stable/>`_ :

.. code-block:: bash

   pip install pyheimdall



.. _pyheimdall_quickstart:

Démarrage
---------

Pour utiliser Heimdall au sein d'un programme Python, il est nécessaire de l'importer.
Le module principal se nomme :py:mod:`heimdall`.

.. code-block:: python

   import heimdall

Lire une base de données au format ``format`` se trouvant à l'adresse ``url`` se fait par un simple appel à la fonction :py:func:`heimdall.getDatabase` :

.. code-block:: python

   tree = heimdall.getDatabase(format=format, url=url)

À l'inverse, convertir une base de données ``tree`` vers le format ``format`` et l'enregistrer à l'adresse ``url`` se fait par un simple appel à la fonction :py:func:`heimdall.createDatabase` :

.. code-block:: python

   heimdall.createDatabase(tree, format=format, url=url)

Par exemple, le programme suivant importe une base de données XML, puis l'exporte sous forme d'une liste de directives SQL :

.. code-block:: python

   import heimdall

   MENAGERIE_XML = 'https://gitlab.huma-num.fr/datasphere/heimdall/python/-/raw/main/tests/examples/menagerie/hera.xml'
   tree = heimdall.getDatabase(format='hera:xml', url=MENAGERIE_XML)
   heimdall.createDatabase(tree, format='sql:mariadb', url='menagerie.sql')



Aller plus loin
---------------

Pour découvrir toutes les fonctionnalités de pyHeimdall, vous pouvez parcourir la `référence <modules.html>`_ (en anglais), et en particulier les modules principaux :py:mod:`heimdall` et :py:mod:`heimdall.util`


.. toctree::
   :hidden:

   modules
   /modindex

.. include:: /doi.rst
