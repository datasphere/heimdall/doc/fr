.. index::
   double: versions; xheimdall
   single: xheimdall
.. _xheimdall:



xHeimdall
==========

|doi-xheimdall|

**xHeimdall** est une implementation de :ref:`Heimdall <home>` en `langage XQuery <https://fr.wikipedia.org/wiki/XQuery>`_.

.. seealso::

   Il existe un autre implémentation de Heimdall en language Python : :ref:`pyheimdall`.
   Si vous avez besoin d'utiliser Heimdall depuis un environnement Python, :ref:`pyheimdall` semble plus appropriée.



.. _xheimdall_install:

Installation
------------

.. note::

   Seul BaseX est aujourd'hui supporté.
   Si `une autre implémentation de XQuery <https://en.wikipedia.org/wiki/XQuery#Implementations>`_ vous intéresse, vous pouvez :ref:`nous contacter <contribuer>`.

Si vous utilisez l'`interface graphique de BaseX <https://docs.basex.org/wiki/Startup>`_, vous pouvez `installer le paquet <https://docs.basex.org/wiki/Repository#Installation>`_ comme ceci :

.. code-block::

   REPO INSTALL https://datasphere.gitpages.huma-num.fr/heimdall/xquery/heimdall.xar

Si BaseX vous renvoie l'erreur « ``[repo:not-found] Package ... heimdall.xar not found.`` », vous pouvez (temporairement) indiquer ``IGNORECERT=true`` [#ignorecert]_ dans votre `fichier de configuration <https://docs.basex.org/wiki/Configuration>`_ et réessayer.

Alternativement, vous pouvez télécharger manuellement le paquet, puis l'installer grâce à son chemin local.
En ligne de commande, cela peut être fait comme ceci :

.. code-block:: bash

   curl -k https://datasphere.gitpages.huma-num.fr/heimdall/xquery/heimdall.xar --output heimdall.xar
   basex -c"<repo-install path='heimdall.xar'/>"



.. _xheimdall_quickstart:

Démarrage
---------

.. note::

   Ces instructions sont valables à partir de xHeimdall version 3.

   Pour la version 2, remplacez simplement :

   * ``heimdall:getDatabase`` par ``heimdall:getDatabaseItems``,
   * ``heimdall:createDatabase`` par ``heimdall.serialize``. 

Lire une base de données au format ``format`` se trouvant à l'adresse ``url`` se fait par un simple appel à la fonction ``heimdall:getDatabase`` :

.. code-block:: xquery

   heimdall:getDatabase(map {
     'url': url,
     'format': format
   })

À l'inverse, convertir une base de données ``$tree`` vers le format ``format`` se fait par un simple appel à la fonction ``heimdall:createDatabase`` :

.. code-block:: xquery

   heimdall:createDatabase($items, map {
     'format': format
   })

Par exemple, le programme suivant importe une base de données MySQL, puis l'exporte en CSV :

.. code-block:: xquery

   let $tree := heimdall:getDatabase(map {
     'url': 'mysql://localhost',
     'database': 'your_db_name_here',
     'format': 'sql:mysql'
     })
   return
   heimdall:createDatabase($tree, map {
     'format': 'csv'
     })



Aller plus loin
---------------

Pour découvrir toutes les fonctionnalités de xHeimdall, vous pouvez parcourir la `référence <https://datasphere.gitpages.huma-num.fr/heimdall/xquery/doc/heimdall.html>`_.


.. include:: /doi.rst

.. [#ignorecert]
   https://docs.basex.org/wiki/Options#IGNORECERT
